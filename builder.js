const IP = require("ip");
const upload_error = require("../../node_error_functions/upload_error");

module.exports = async (columns, database, table, session) => {
  try {
    let create_column_string = ``;

    for (let i = 0; i < columns.columns.length; i++) {
      create_column_string = `${create_column_string} ${columns.columns[i].label} ${columns.columns[i].type},`;
    }
    create_column_string.substring(0, create_column_string.length - 1);
    const TABLE_SQL_CREATE = `
    CREATE TABLE IF NOT EXISTS ${database}.${table} (${create_column_string} ${
      columns.primary_key
    } ${columns.foreign_key ? `, ${columns.foreign_key}` : ``});

    `;
    console.log(TABLE_SQL_CREATE);
    await session.sql(TABLE_SQL_CREATE).execute();

    for (let i = 0; i < columns.columns.length; i++) {
      let result = await session
        .sql(
          `SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = '${database}' AND TABLE_NAME = '${table}' AND COLUMN_NAME = '${columns.columns[i].label}';`
        )
        .execute();

      result = result.fetchOne();

      if (!result) {
        console.log(
          `ALTER TABLE ${database}.${table} ADD ${columns.columns[i].label} ${columns.columns[i].type};`
        );
        await session
          .sql(
            `ALTER TABLE ${database}.${table} ADD ${columns.columns[i].label} ${columns.columns[i].type};`
          )
          .execute();
      }
    }
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Building Table",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
